# nest-streaming-module

## Prerequisites
* CMake >= 3.6.0 (https://cmake.org/)
* Python 2.7 (https://www.python.org/)
* NEST: the latest release of NEST does currently not support modules. Thus, a custom version is required that can be found [here](https://github.com/jougs/nest-simulator/tree/feature/custom-recording-backend). You need to checkout and build the branch `feature/custom-recording-backend`

## Building
1. Create a build directory
2. Open terminal in build directory
3. Run CMake: `cmake -Dwith-nest=${PATH_TO_NEST_INSTALLATION} ${PATH_TO_NEST_STREAMING_MODULE_SOURCE}`
4. Run Conan: `conan install ${PATH_TO_NEST_STREAMING_MODULE_SOURCE} --build=missing`
5. Build: `cmake --build .`
6. Install: `cmake --build . --target install`

## How to use the nest-streaming-module?
The *nest-streaming-module* registers a new RecordingBackend to the simulator. When registering a `RecordingDevice` using PyNEST the used recording backend can be specified in the `record_to` parameter. If you want to stream the data, set this parameter to `['streaming']`. Clients that receive the data can be either written in Python or C++. See https://devhub.vr.rwth-aachen.de/VR-Group/nesci-contra-demos for examples.