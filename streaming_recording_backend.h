/*
 *  recording_backend_stream_niv.h
 *
 *  This file is part of NEST.
 *
 *  Copyright (C) 2004 The NEST Initiative
 *
 *  NEST is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  NEST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NEST.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RECORDING_BACKEND_STREAM_NIV_H
#define RECORDING_BACKEND_STREAM_NIV_H

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "nest_types.h"
#include "recording_backend.h"

#include "contra/relay.hpp"
#include "contra/shared_memory/shared_memory_transport.hpp"
#include "nesci/producer/device.hpp"

namespace streamingnest {

class StreamingRecordingBackend : public nest::RecordingBackend {
 public:
  StreamingRecordingBackend();
  ~StreamingRecordingBackend() throw() {}

  void enroll(const nest::RecordingDevice &, const std::vector<Name> &,
              const std::vector<Name> &) override;

  void initialize() override;

  void prepare() override;
  void cleanup() override;
  void post_run_cleanup() override;

  void finalize() override;
  void synchronize() override;

  void clear(const nest::RecordingDevice&) override;

  void write(const nest::RecordingDevice &, const nest::Event &,
             const std::vector<double> &, const std::vector<long> &) override;

 private:
  std::mutex relay_mutex_;
  contra::Relay<contra::SharedMemoryTransport> relay_;

  std::mutex write_mutex_;

  std::map<std::thread::id,
           std::map<std::string, std::unique_ptr<nesci::producer::Device>>>
      devices_;
  std::mutex enroll_mutex_;
};
}  // namespace streamingnest

#endif  // RECORDING_BACKEND_STREAM_NIV_H
