#-------------------------------------------------------------------------------
# QVTK-Demo
#
# Copyright (c) 2017-2018 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualization Group.
#-------------------------------------------------------------------------------
#                                  License
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

This is a brief summary on how to get the QVTK-Demo running.

Prerequisites:
	-QT (Version 5.9 or higher is advised! -older versions might work aswell but havent been tested).
		-Get this first and build it.
	-The Visualization Toolkit(VTK) (The demo was originally build with version 8.0.1. Newer versions should work too but havent been tested).
		-Ŵhen building VTK with CMake make sure you enable VTK_BUILD_QT_DESIGNER_PLUGIN, VTK_Group_Qt and set VTK_QT_VERSION to 5.

Installing:
When running CMake enable BUILD_QVTK_DEMO and BUILD_BRUNEL_SIMULATION.

Running the Demo:
To run the Demo start both, the QVTK-Demo(qvtk-app) and the Brunel Simulation(run_sim.sh) from your build folder.

 
