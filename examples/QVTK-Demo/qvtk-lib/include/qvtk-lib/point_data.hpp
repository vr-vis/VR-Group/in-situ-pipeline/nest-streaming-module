//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QVTK_LIB_INCLUDE_QVTK_LIB_POINT_DATA_HPP_
#define QVTK_LIB_INCLUDE_QVTK_LIB_POINT_DATA_HPP_

#include <vector>

#include "include/qvtk-lib/suppress_warnings.hpp"
SUPPRESS_WARNINGS_BEGIN
#include "qvtk-lib/data_stream.hpp"

#include "vtkDataObject.h"
#include "vtkFloatArray.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"
SUPPRESS_WARNINGS_END

namespace vtkexp
{

class PointData
{
public:
  PointData(int num_x, int num_y, int num_z);
  ~PointData() {}

  void Update(int time_step = -1);
  vtkSmartPointer<vtkPolyData> GetPolyPoints();
  vtkSmartPointer<vtkFloatArray> GetColors() const;

  int GetEndTime() const;
  int GetStartTime() const;

private:
  void AttachColorsToPoints(const std::vector<double> &colors);
  static vtkSmartPointer<vtkPoints> CreatePoints(int num_x, int num_y,
                                                 int num_z);
  void Initialize(int x, int y, int z);
  void ColorsModified();

  DataStream *stream_;

  int num_points_;
  vtkSmartPointer<vtkPolyData> points_polydata_;
  vtkSmartPointer<vtkFloatArray> pointcolors_;
};

} // namespace vtkexp

#endif // QVTK_LIB_INCLUDE_QVTK_LIB_POINT_DATA_HPP_
