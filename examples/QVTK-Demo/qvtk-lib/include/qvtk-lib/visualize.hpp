//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QVTK_LIB_INCLUDE_QVTK_LIB_VISUALIZE_HPP_
#define QVTK_LIB_INCLUDE_QVTK_LIB_VISUALIZE_HPP_

#include <string>

#include "include/qvtk-lib/suppress_warnings.hpp"
SUPPRESS_WARNINGS_BEGIN
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include "qvtk-lib/interaction.hpp"
#include "qvtk-lib/point_data.hpp"

#include "vtkActor.h"
#include "vtkAxesActor.h"
#include "vtkColorTransferFunction.h"
#include "vtkGenericOpenGLRenderWindow.h"
#include "vtkOrientationMarkerWidget.h"
#include "vtkPointGaussianMapper.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkScalarBarActor.h"
#include "vtkScalarBarWidget.h"
#include "vtkSmartPointer.h"
#pragma GCC diagnostic pop
SUPPRESS_WARNINGS_END

namespace vtkexp
{

class Visualize
{
public:
  explicit Visualize(PointData *points);
  ~Visualize() {}

  void Initialize(vtkSmartPointer<vtkGenericOpenGLRenderWindow> renderwindow,
                  vtkSmartPointer<vtkRenderWindowInteractor> interactor);
  vtkSmartPointer<vtkRenderer> GetRenderer();

  void SwitchMapper(int rendertype);

private:
  void SetUpAxes();
  void SetUpInteractor();
  void SetUpLegend();
  void SetUpMapper();
  void SetUpRenderer();
  void SetUpScene();
  void SetUpShaders();
  void SetUpTransferFunction();

  PointData *points_;

  std::string point_shader_;
  std::string sphere_shader_;

  vtkSmartPointer<vtkActor> actor_;
  vtkSmartPointer<vtkRenderer> renderer_;
  vtkSmartPointer<vtkRenderWindowInteractor> interactor_;
  vtkSmartPointer<vtkGenericOpenGLRenderWindow> renderwindow_;

  vtkSmartPointer<vtkPointGaussianMapper> mapper_;
  vtkSmartPointer<vtkColorTransferFunction> transfer_function_;

  vtkSmartPointer<vtkAxesActor> axes_;
  vtkSmartPointer<vtkOrientationMarkerWidget> axes_widget_;
  vtkSmartPointer<vtkScalarBarWidget> scalar_bar_widget_;
  vtkSmartPointer<vtkScalarBarActor> scalar_bar_;
};

} // namespace vtkexp

#endif // QVTK_LIB_INCLUDE_QVTK_LIB_VISUALIZE_HPP_
