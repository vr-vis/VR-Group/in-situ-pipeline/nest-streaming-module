//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QVTK_LIB_INCLUDE_QVTK_LIB_INTERACTION_HPP_
#define QVTK_LIB_INCLUDE_QVTK_LIB_INTERACTION_HPP_

#include <string>

#include "include/qvtk-lib/suppress_warnings.hpp"
SUPPRESS_WARNINGS_BEGIN
#include <vtkActor.h>
#include <vtkDataSetMapper.h>
#include <vtkMapper.h>
#include <vtkPointGaussianMapper.h>
#include <vtkPointPicker.h>
#include <vtkUnstructuredGrid.h>
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkPolyData.h"
SUPPRESS_WARNINGS_END

namespace vtkexp
{

class Interaction : public vtkInteractorStyleTrackballCamera
{
  public:
    static Interaction *New();
    vtkTypeMacro(Interaction, vtkInteractorStyleTrackballCamera)

        Interaction();

    virtual void OnLeftButtonUp();

    void SetUpShaders();
    void SetUpActor();
    void SetUpMapper();

    double GetNeuronValue();
    vtkIdType GetNeuronId();

    void SetData(vtkSmartPointer<vtkPolyData> data);
    void SwitchMapper(int rendertype);
    void ExtractSelection();
    void HighlightSelection();
    void Deselect();
    void Pick();

  private:
    double neuron_value_;
    vtkIdType id_;
    vtkSmartPointer<vtkUnstructuredGrid> selected_;

    vtkSmartPointer<vtkPolyData> data_;

    vtkSmartPointer<vtkActor> actor_;
    vtkSmartPointer<vtkPointGaussianMapper> mapper_;
    vtkSmartPointer<vtkPointPicker> picker_;

    std::string point_shader_;
    std::string sphere_shader_;
};

} // namespace vtkexp

#endif // QVTK_LIB_INCLUDE_QVTK_LIB_INTERACTION_HPP_
