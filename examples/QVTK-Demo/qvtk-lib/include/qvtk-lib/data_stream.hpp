//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QVTK_LIB_INCLUDE_QVTK_LIB_DATA_STREAM_HPP_
#define QVTK_LIB_INCLUDE_QVTK_LIB_DATA_STREAM_HPP_

#include <vector>

#include "conduit/conduit_node.hpp"
//#include "contra/boost-shmem/shared_memory_transport.hpp"
#include "contra/relay.hpp"
#include "contra/zmq/zeromq_transport.hpp"

#include "nesci/consumer/nest_multimeter.hpp"

namespace vtkexp
{

class DataStream
{
public:
  DataStream();
  ~DataStream() {}

  std::vector<double> GetMultValuesAt(int time_step);
  std::vector<double> GetTimeSteps();

private:
  void SetUpStream();
  void Update(int time_step);

  std::vector<double> mult_values_;

  contra::Relay<contra::ZMQTransport> *relay_;
  nesci::consumer::NestMultimeter *multimeter_;

  conduit::Node node_;
};

} // namespace vtkexp

#endif // QVTK_LIB_INCLUDE_QVTK_LIB_DATA_STREAM_HPP_
