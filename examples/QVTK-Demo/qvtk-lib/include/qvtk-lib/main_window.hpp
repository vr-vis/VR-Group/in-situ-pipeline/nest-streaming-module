//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QVTK_LIB_INCLUDE_QVTK_LIB_MAIN_WINDOW_HPP_
#define QVTK_LIB_INCLUDE_QVTK_LIB_MAIN_WINDOW_HPP_

#include "include/qvtk-lib/suppress_warnings.hpp"

#include "qvtk-lib/animate.hpp"
#include "qvtk-lib/main_widget.hpp"
SUPPRESS_WARNINGS_BEGIN
#include "qvtk-lib/point_data.hpp"
SUPPRESS_WARNINGS_END

#include "QComboBox"
#include "QGridLayout"
#include "QLabel"
#include "QLineEdit"
#include "QMainWindow"
#include "QPushButton"
#include "QSlider"
#include "QTimer"

namespace vtkexp
{

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(PointData *points);
  ~MainWindow() {}

  QLineEdit *slider_line_edit_;

public slots: // NOLINT
  void Rerender();
  void Realtime();
  void PlayPause();
  void IncrementSlider();
  void UpdateSlider();
  void AdjustSlider();
  void SliderValue();
  void SliderValue(int time_step);

private:
  void ConnectSignals();
  void InitSlider(QSlider *slider);
  void SetUpButtons();
  void SetUpContent();
  void SetUpLayout();
  void SetUpLineEdit();
  void SetUpSliders();
  void SetUpVisualization();
  void StartAnimation();

  Animate *animation_;
  PointData *points_;

  QWidget *dummywidget_;
  MainWidget *mainwidget_;
  QComboBox *render_type_;
  QComboBox *speed_menu_;
  QPushButton *rt_button_;
  QPushButton *play_button_;
  QGridLayout *grid_;
  QLabel *time_label_;
  QSlider *time_slider_;

  bool realtime_{true};
  bool play_{true};
};

} // namespace vtkexp

#endif // QVTK_LIB_INCLUDE_QVTK_LIB_MAIN_WINDOW_HPP_
