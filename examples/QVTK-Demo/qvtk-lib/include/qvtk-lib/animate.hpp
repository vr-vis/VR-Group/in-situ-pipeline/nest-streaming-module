//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QVTK_LIB_INCLUDE_QVTK_LIB_ANIMATE_HPP_
#define QVTK_LIB_INCLUDE_QVTK_LIB_ANIMATE_HPP_

#include "QObject"
#include "QTimer"

#include "include/qvtk-lib/suppress_warnings.hpp"
SUPPRESS_WARNINGS_BEGIN
#include "qvtk-lib/point_data.hpp"
SUPPRESS_WARNINGS_END

namespace vtkexp
{

class Animate : public QObject
{
  Q_OBJECT

public:
  explicit Animate(PointData *points);
  ~Animate() {}

  void StartTimer();

signals:
  void DataChanged();
  void DataChanged(int time_step);
  void NextStep();
  void LastStep();
  void SwitchRealtime();
  void SwitchPlayPause();

public slots: // NOLINT
  void ChangeData(int timestep = -1);
  void IncreaseSpeed();
  void DecreaseSpeed();
  void Iterate();
  void RealtimeButton();
  void PlayButton();
  void SpeedMenu(int index);

private:
  PointData *points_;
  QTimer *rt_timer_;
  QTimer *play_timer_;
  int play_speed_ = 1000;
};

} // namespace vtkexp

#endif // QVTK_LIB_INCLUDE_QVTK_LIB_ANIMATE_HPP_
