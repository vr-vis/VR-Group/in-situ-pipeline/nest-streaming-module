//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QVTK_LIB_INCLUDE_QVTK_LIB_MAIN_WIDGET_HPP_
#define QVTK_LIB_INCLUDE_QVTK_LIB_MAIN_WIDGET_HPP_

#include "qvtk-lib/visualize.hpp"

#include "qvtk-lib/interaction.hpp"

#include "QObject"

#include "include/qvtk-lib/suppress_warnings.hpp"
SUPPRESS_WARNINGS_BEGIN
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include "QVTKOpenGLWidget.h"
#include "vtkGenericOpenGLRenderWindow.h"
#pragma GCC diagnostic pop
SUPPRESS_WARNINGS_END

namespace vtkexp
{

class MainWidget : public QVTKOpenGLWidget
{
  Q_OBJECT

public:
  explicit MainWidget(PointData *points, QVTKOpenGLWidget *parent = 0);
  ~MainWidget() {}

  void Rerender();

public slots: // NOLINT
  void ChangeRendertype(int rendertype);

protected:
  void mouseReleaseEvent(QMouseEvent *event) override;

private:
  Visualize *vispoints_;
  vtkSmartPointer<Interaction> style_;
  vtkSmartPointer<vtkGenericOpenGLRenderWindow> renderwindow_;
};

} // namespace vtkexp

#endif // QVTK_LIB_INCLUDE_QVTK_LIB_MAIN_WIDGET_HPP_
