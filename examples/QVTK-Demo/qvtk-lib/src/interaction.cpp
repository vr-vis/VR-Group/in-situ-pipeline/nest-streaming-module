//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include "qvtk-lib/interaction.hpp"

#include <string>

SUPPRESS_WARNINGS_BEGIN
#include "vtkExtractSelection.h"
#include "vtkGeometryFilter.h"
#include "vtkHardwareSelector.h"
#include "vtkIdTypeArray.h"
#include "vtkPointData.h"
#include "vtkProperty.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRendererCollection.h"
#include "vtkSelection.h"
#include "vtkSelectionNode.h"
#include "vtkSmartPointer.h"
#include "vtkVertexGlyphFilter.h"
SUPPRESS_WARNINGS_END

namespace vtkexp
{

vtkStandardNewMacro(Interaction)

    Interaction::Interaction()
{
  SetUpShaders();
  SetUpMapper();
  SetUpActor();
}

void Interaction::SetUpMapper()
{
  mapper_ = vtkSmartPointer<vtkPointGaussianMapper>::New();
  mapper_->SetSplatShaderCode(point_shader_.c_str());
  mapper_->ScalarVisibilityOff();
  mapper_->SetScaleFactor(0.35);
  mapper_->UseLookupTableScalarRangeOn();
  mapper_->EmissiveOff();
  mapper_->Update();
}

void Interaction::SetUpActor()
{
  actor_ = vtkSmartPointer<vtkActor>::New();
  actor_->SetMapper(mapper_);
  actor_->GetProperty()->SetColor(0.0, 1.0, 0.0);
  actor_->GetProperty()->SetEdgeVisibility(1);
}

void Interaction::OnLeftButtonUp()
{
  Pick();

  vtkSmartPointer<vtkHardwareSelector> selector =
      vtkSmartPointer<vtkHardwareSelector>::New();
  selector->SetRenderer(
      this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer());
  int *temp = this->Interactor->GetRenderWindow()->GetSize();
  unsigned int windowSize[4];
  windowSize[0] = temp[2];
  windowSize[1] = temp[3];
  windowSize[2] = temp[0];
  windowSize[3] = temp[1];
  /*
  for(unsigned int i = 0; i < 4; i++)
    {
    windowSize[i] = temp[i];
    }
  */
  selector->SetArea(windowSize);
  selector->SetFieldAssociation(vtkDataObject::FIELD_ASSOCIATION_POINTS);
  vtkSelection *selection = selector->Select();
  std::cout << "Selection has " << selection->GetNumberOfNodes() << " nodes."
            << std::endl;

  vtkSmartPointer<vtkExtractSelection> extractSelection =
      vtkSmartPointer<vtkExtractSelection>::New();
  extractSelection->SetInputData(0, data_);
  extractSelection->Update();

  vtkSmartPointer<vtkDataSetMapper> mapper =
      vtkSmartPointer<vtkDataSetMapper>::New();
  mapper->SetInputConnection(extractSelection->GetOutputPort());

  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
  actor->GetProperty()->SetColor(1, 0, 0);
  GetDefaultRenderer()->AddActor(actor);

  if (id_ != -1)
  {
    ExtractSelection();
    HighlightSelection();
  }
  else
  {
    Deselect();
  }
  vtkInteractorStyleTrackballCamera::OnLeftButtonUp();
}

void Interaction::Pick()
{
  int *pos = GetInteractor()->GetEventPosition();

  picker_ = vtkSmartPointer<vtkPointPicker>::New();
  picker_->SetTolerance(0.015);

  picker_->Pick(pos[0], pos[1], 0, GetDefaultRenderer());
  id_ = picker_->GetPointId();
  std::cout << id_ << std::endl;
}

void Interaction::ExtractSelection()
{
  vtkSmartPointer<vtkIdTypeArray> ids = vtkSmartPointer<vtkIdTypeArray>::New();
  ids->SetNumberOfComponents(1);
  ids->InsertNextValue(picker_->GetPointId());

  vtkSmartPointer<vtkSelectionNode> selectionNode =
      vtkSmartPointer<vtkSelectionNode>::New();
  selectionNode->SetFieldType(vtkSelectionNode::POINT);
  selectionNode->SetContentType(vtkSelectionNode::INDICES);
  selectionNode->SetSelectionList(ids);

  vtkSmartPointer<vtkSelection> selection =
      vtkSmartPointer<vtkSelection>::New();
  selection->AddNode(selectionNode);

  vtkSmartPointer<vtkExtractSelection> extract_selection =
      vtkSmartPointer<vtkExtractSelection>::New();
  extract_selection->SetInputData(0, data_);
  extract_selection->SetInputData(1, selection);
  extract_selection->Update();

  selected_ = vtkSmartPointer<vtkUnstructuredGrid>::New();
  selected_->ShallowCopy(extract_selection->GetOutput());

  neuron_value_ = selected_->GetPointData()->GetScalars()->GetComponent(0, 0);
}

void Interaction::HighlightSelection()
{
  vtkSmartPointer<vtkGeometryFilter> geometryFilter =
      vtkSmartPointer<vtkGeometryFilter>::New();
  geometryFilter->SetInputData(selected_);
  geometryFilter->Update();

  vtkSmartPointer<vtkVertexGlyphFilter> glyphFilter =
      vtkSmartPointer<vtkVertexGlyphFilter>::New();
  glyphFilter->SetInputData(geometryFilter->GetOutput());
  glyphFilter->Update();

  mapper_->SetInputData(glyphFilter->GetOutput());

  CurrentRenderer->AddActor(actor_);
  GetInteractor()->GetRenderWindow()->Render();
}

void Interaction::Deselect()
{
  vtkSmartPointer<vtkPolyData> empty = vtkSmartPointer<vtkPolyData>::New();

  mapper_->SetInputData(empty);
  mapper_->Update();

  CurrentRenderer->AddActor(actor_);
  GetInteractor()->GetRenderWindow()->Render();
}

void Interaction::SetData(vtkSmartPointer<vtkPolyData> data) { data_ = data; }

void Interaction::SwitchMapper(int rendertype)
{
  if (rendertype == 0)
  {
    mapper_->SetSplatShaderCode(point_shader_.c_str());
  }
  else if (rendertype == 1)
  {
    mapper_->SetSplatShaderCode(sphere_shader_.c_str());
  }
}

double Interaction::GetNeuronValue() { return neuron_value_; }

vtkIdType Interaction::GetNeuronId() { return id_; }

void Interaction::SetUpShaders()
{
  sphere_shader_ =
      "//VTK::Color::Impl\n"
      "float dist = dot(offsetVCVSOutput.xy,offsetVCVSOutput.xy);\n"
      "if (dist > 0.9) {\n"
      "  discard;\n"
      "} else {\n"
      "  float scale = (1.0 - dist);\n"
      "  ambientColor *= 0.5;\n"
      "  diffuseColor *= 0.7;\n"
      "}\n";

  point_shader_ =
      "//VTK::Color::Impl\n"
      "ambientColor *= 0.5;\n"
      "diffuseColor *= 0.7;\n"
      "if (abs(offsetVCVSOutput.x) > 0.3 || abs(offsetVCVSOutput.y) > 0.3) "
      "{\n"
      "  discard;\n"
      "}\n"
      "if (abs(offsetVCVSOutput.x) < 0.0 && abs(offsetVCVSOutput.y) < 0.0) "
      "{\n"
      "  discard;\n"
      "}\n";
}
} // namespace vtkexp
