//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include "qvtk-lib/animate.hpp"

namespace vtkexp
{

Animate::Animate(PointData *points) : points_(points)
{
  rt_timer_ = new QTimer(this);
  play_timer_ = new QTimer(this);
  connect(rt_timer_, SIGNAL(timeout()), this, SLOT(ChangeData()));
  connect(play_timer_, SIGNAL(timeout()), this, SLOT(Iterate()));
}

void Animate::ChangeData(int time_step)
{
  points_->Update(time_step);
  emit DataChanged();
}

void Animate::IncreaseSpeed() { play_speed_ += 1; }

void Animate::DecreaseSpeed() { play_speed_ -= 1; }

void Animate::Iterate()
{
  rt_timer_->isActive() ? emit LastStep() : emit NextStep();
}

void Animate::StartTimer()
{
  rt_timer_->start(1);
  play_timer_->start(play_speed_);
}

void Animate::RealtimeButton()
{
  if (rt_timer_->isActive())
  {
    rt_timer_->stop();
    emit SwitchRealtime();
  }
  else
  {
    rt_timer_->start(1);
    emit SwitchRealtime();
  }
}

void Animate::SpeedMenu(int index)
{
  switch (index)
  {
  case 0:
    play_speed_ = 1000;
    break;
  case 1:
    play_speed_ = 1000 / 5;
    break;
  case 2:
    play_speed_ = 1000 / 10;
    break;
  case 3:
    play_speed_ = 1000 / 50;
    break;
  case 4:
    play_speed_ = 1000 / 100;
    break;
  default:
    play_speed_ = 1000;
    break;
  }
  play_timer_->setInterval(play_speed_);
}

void Animate::PlayButton()
{
  play_timer_->isActive() ? play_timer_->stop()
                          : play_timer_->start(play_speed_);
  emit SwitchPlayPause();
}

} // namespace vtkexp
