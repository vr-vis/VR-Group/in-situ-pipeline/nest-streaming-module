//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <vector>

#include "include/qvtk-lib/suppress_warnings.hpp"
SUPPRESS_WARNINGS_BEGIN
#include "qvtk-lib/point_data.hpp"

#include "vtkCellArray.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include "vtkPointData.h"
#pragma GCC diagnostic pop
SUPPRESS_WARNINGS_END

namespace vtkexp
{

PointData::PointData(int num_x, int num_y, int num_z)
    : num_points_(num_x * num_y * num_z)
{
  Initialize(num_x, num_y, num_z);
}

void PointData::Initialize(int x, int y, int z)
{
  points_polydata_ = vtkSmartPointer<vtkPolyData>::New();
  points_polydata_->SetPoints(CreatePoints(x, y, z));

  pointcolors_ = vtkSmartPointer<vtkFloatArray>::New();
  pointcolors_->SetName("colors");
  pointcolors_->SetNumberOfTuples(num_points_);

  std::vector<double> init(num_points_);
  std::fill(init.begin(), init.end(), 0);
  AttachColorsToPoints(init);

  stream_ = new DataStream();
}

vtkSmartPointer<vtkPoints> PointData::CreatePoints(int num_x, int num_y,
                                                   int num_z)
{
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  for (int i = 0; i < num_x; ++i)
  {
    for (int j = 0; j < num_y; ++j)
    {
      for (int k = 0; k < num_z; ++k)
      {
        points->InsertNextPoint(i, j, k);
      }
    }
  }
  return points;
}

void PointData::ColorsModified()
{
  points_polydata_->GetPointData()->GetScalars()->Modified();
}

void PointData::AttachColorsToPoints(const std::vector<double> &scalars)
{
  if (!scalars.empty())
  {
    for (int i = 0; i < num_points_; ++i)
    {
      pointcolors_->SetValue(
          i, static_cast<float>(scalars.at(static_cast<int>(i))));
    }
    points_polydata_->GetPointData()->SetScalars(pointcolors_);
  }
}

void PointData::Update(int time_step)
{
  AttachColorsToPoints(stream_->GetMultValuesAt(time_step));
  ColorsModified();
}

vtkSmartPointer<vtkPolyData> PointData::GetPolyPoints()
{
  return points_polydata_;
}

vtkSmartPointer<vtkFloatArray> PointData::GetColors() const
{
  return pointcolors_;
}

int PointData::GetStartTime() const
{
  if (!stream_->GetTimeSteps().empty())
  {
    return static_cast<int>(stream_->GetTimeSteps().front());
  }
  return 0;
}

int PointData::GetEndTime() const
{
  if (!stream_->GetTimeSteps().empty())
  {
    return static_cast<int>(stream_->GetTimeSteps().back());
  }
  return 100;
}

} // namespace vtkexp
