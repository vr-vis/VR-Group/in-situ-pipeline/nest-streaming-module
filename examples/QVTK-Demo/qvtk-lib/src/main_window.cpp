//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include "qvtk-lib/main_window.hpp"

namespace vtkexp
{

MainWindow::MainWindow(PointData *points) : points_(points)
{
  setWindowTitle("QVTK-Neuron-Viewer");
  SetUpVisualization();
  SetUpContent();
  setCentralWidget(dummywidget_);
  ConnectSignals();
  StartAnimation();
}

void MainWindow::SetUpVisualization()
{
  animation_ = new Animate(points_);
  mainwidget_ = new MainWidget(points_);
}

void MainWindow::SetUpContent()
{
  SetUpSliders();
  SetUpLineEdit();
  SetUpButtons();
  SetUpLayout();
}

void MainWindow::SetUpLayout()
{
  dummywidget_ = new QWidget;
  grid_ = new QGridLayout;
  grid_->addWidget(mainwidget_, 0, 0, 1, -1);
  grid_->addWidget(time_label_, 1, 0, 1, 1);
  grid_->addWidget(time_slider_, 1, 1, 1, 1);
  grid_->addWidget(slider_line_edit_, 1, 2, 1, 10);
  grid_->addWidget(render_type_, 1, 14, 1, 1);
  grid_->addWidget(rt_button_, 1, 15, 1, 1);
  grid_->addWidget(play_button_, 1, 13, 1, 1);
  grid_->addWidget(speed_menu_, 1, 12, 1, 1);
  dummywidget_->setLayout(grid_);

  resize(800, 600);
}

void MainWindow::SetUpLineEdit()
{
  slider_line_edit_ = new QLineEdit();
  slider_line_edit_->setText(std::to_string(0).c_str());
}

void MainWindow::SetUpSliders()
{
  time_slider_ = new QSlider(Qt::Horizontal);
  time_label_ = new QLabel("Time:");
  InitSlider(time_slider_);
  time_slider_->setEnabled(0);
}

void MainWindow::InitSlider(QSlider *slider)
{
  slider->setFocusPolicy(Qt::StrongFocus);
  slider->setTickPosition(QSlider::TicksAbove);
  slider->setTickInterval(50);
  slider->setSingleStep(20);
  slider->setMinimum(0);
  slider->setMaximum(100);
  slider->setValue(100);
}

void MainWindow::SetUpButtons()
{
  rt_button_ = new QPushButton("Replay", this);
  rt_button_->setFixedSize(QSize(80, 32));
  play_button_ = new QPushButton("⏸", this);
  play_button_->setFixedSize(QSize(32, 32));
  render_type_ = new QComboBox(this);
  render_type_->setFixedSize(QSize(80, 32));
  render_type_->addItems({"Points", "Spheres"});
  speed_menu_ = new QComboBox(this);
  speed_menu_->addItems({"1/sec", "5/sec", "10/sec", "50/sec", "100/sec"});
}

void MainWindow::StartAnimation() { animation_->StartTimer(); }

void MainWindow::ConnectSignals()
{
  connect(speed_menu_, SIGNAL(activated(int)), animation_,
          SLOT(SpeedMenu(int)));
  connect(rt_button_, SIGNAL(clicked()), animation_, SLOT(RealtimeButton()));
  connect(play_button_, SIGNAL(clicked()), animation_, SLOT(PlayButton()));
  connect(time_slider_, SIGNAL(valueChanged(int)), animation_,
          SLOT(ChangeData(int)));
  connect(time_slider_, SIGNAL(valueChanged(int)), SLOT(SliderValue(int)));
  connect(slider_line_edit_, SIGNAL(editingFinished()), SLOT(SliderValue()));
  connect(render_type_, SIGNAL(currentIndexChanged(int)), mainwidget_,
          SLOT(ChangeRendertype(int)));
  connect(animation_, SIGNAL(SwitchRealtime()), this, SLOT(Realtime()));
  connect(animation_, SIGNAL(SwitchPlayPause()), this, SLOT(PlayPause()));
  connect(animation_, SIGNAL(DataChanged()), this, SLOT(Rerender()));
  connect(animation_, SIGNAL(DataChanged()), this, SLOT(AdjustSlider()));
  connect(animation_, SIGNAL(NextStep()), SLOT(IncrementSlider()));
  connect(animation_, SIGNAL(LastStep()), SLOT(UpdateSlider()));
}

void MainWindow::Rerender() { mainwidget_->Rerender(); }

void MainWindow::IncrementSlider()
{
  int newval;
  if (time_slider_->value() + 1 < time_slider_->maximum())
  {
    newval = time_slider_->value() + 1;
  }
  else
  {
    newval = time_slider_->maximum();
  }

  time_slider_->setValue(newval);
  slider_line_edit_->setText(std::to_string(newval).c_str());
}

void MainWindow::UpdateSlider()
{
  time_slider_->setValue(points_->GetEndTime());
  slider_line_edit_->setText(std::to_string(time_slider_->value()).c_str());
}

void MainWindow::AdjustSlider()
{
  time_slider_->setMinimum(points_->GetStartTime());
  time_slider_->setMaximum(points_->GetEndTime());
}

void MainWindow::SliderValue(int time_step)
{
  slider_line_edit_->setText(std::to_string(time_step).c_str());
  time_slider_->setMinimum(points_->GetStartTime());
  time_slider_->setMaximum(points_->GetEndTime());
}

void MainWindow::SliderValue()
{
  time_slider_->setValue(slider_line_edit_->text().toInt());
}

void MainWindow::Realtime()
{
  if (realtime_)
  {
    realtime_ = false;
    rt_button_->setText("Realtime");
    time_slider_->setMinimum(points_->GetStartTime());
    time_slider_->setMaximum(points_->GetEndTime());
    time_slider_->setEnabled(1);
  }
  else
  {
    realtime_ = true;
    time_slider_->setValue(points_->GetEndTime());
    rt_button_->setText("Replay");
    time_slider_->setEnabled(0);
  }
}

void MainWindow::PlayPause()
{
  if (play_)
  {
    play_ = false;
    play_button_->setText("▶");
    SliderValue(slider_line_edit_->text().toInt());
  }
  else
  {
    play_ = true;
    play_button_->setText("⏸");
  }
}

} // namespace vtkexp
