//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include "qvtk-lib/visualize.hpp"

#include <string>

namespace vtkexp
{

Visualize::Visualize(PointData *points) { points_ = points; }

vtkSmartPointer<vtkRenderer> Visualize::GetRenderer() { return renderer_; }

void Visualize::Initialize(
    vtkSmartPointer<vtkGenericOpenGLRenderWindow> renderwindow,
    vtkSmartPointer<vtkRenderWindowInteractor> interactor)
{
  renderwindow_ = renderwindow;
  interactor_ = interactor;
  SetUpTransferFunction();
  SetUpMapper();
  SetUpScene();
  SetUpRenderer();
  SetUpAxes();
  SetUpLegend();
}

void Visualize::SetUpLegend()
{
  scalar_bar_widget_ = vtkSmartPointer<vtkScalarBarWidget>::New();
  scalar_bar_widget_->SetInteractor(interactor_);
  scalar_bar_widget_->SetEnabled(true);

  scalar_bar_ = scalar_bar_widget_->GetScalarBarActor();
  // scalar_bar_->SetTitle("Legend");
  scalar_bar_->SetLookupTable(transfer_function_);
  scalar_bar_->SetOrientationToVertical();
  scalar_bar_->SetNumberOfLabels(0);

  scalar_bar_->SetPosition2(0, 0);
  scalar_bar_->SetBarRatio(0.6);
}

void Visualize::SetUpAxes()
{
  axes_ = vtkSmartPointer<vtkAxesActor>::New();

  axes_widget_ = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
  axes_widget_->SetOutlineColor(0.9300, 0.5700, 0.1300);
  axes_widget_->SetOrientationMarker(axes_);
  axes_widget_->SetInteractor(interactor_);
  axes_widget_->SetViewport(0.0, 0.0, 0.2, 0.2);
  axes_widget_->SetEnabled(1);
  axes_widget_->InteractiveOn();

  renderer_->ResetCamera();
}

void Visualize::SetUpScene()
{
  actor_ = vtkSmartPointer<vtkActor>::New();
  actor_->SetMapper(mapper_);
}

void Visualize::SetUpRenderer()
{
  renderer_ = vtkSmartPointer<vtkRenderer>::New();
  renderwindow_->AddRenderer(renderer_);
  renderer_->AddActor(actor_);
  interactor_->GetInteractorStyle()->SetDefaultRenderer(renderer_);

  renderer_->GradientBackgroundOn();
  renderer_->SetBackground(0.3, 0.4, 0.5);
  renderer_->SetBackground2(0.1, 0.2, 0.3);
}

void Visualize::SetUpTransferFunction()
{
  transfer_function_ = vtkSmartPointer<vtkColorTransferFunction>::New();
  transfer_function_->SetColorSpaceToDiverging();
  transfer_function_->AddRGBPoint(-15, 0, 0, 1);
  transfer_function_->AddRGBPoint(5, 1, 0, 0);
}

void Visualize::SetUpMapper()
{
  SetUpShaders();

  mapper_ = vtkSmartPointer<vtkPointGaussianMapper>::New();
  mapper_->SetInputData(points_->GetPolyPoints());
  mapper_->SetSplatShaderCode(point_shader_.c_str());
  mapper_->SetLookupTable(transfer_function_);
  mapper_->SetScaleFactor(0.35);
  mapper_->UseLookupTableScalarRangeOn();
  mapper_->EmissiveOff();
  mapper_->Update();
}

void Visualize::SetUpShaders()
{
  point_shader_ =
      "//VTK::Color::Impl\n"
      "ambientColor *= 1.0;\n"
      "diffuseColor *= 1.0;\n"
      "if (abs(offsetVCVSOutput.x) > 0.2 || abs(offsetVCVSOutput.y) > 0.2) "
      "{\n"
      "  discard;\n"
      "}\n";

  sphere_shader_ =
      "//VTK::Color::Impl\n"
      "float dist = dot(offsetVCVSOutput.xy,offsetVCVSOutput.xy);\n"
      "if (dist > 0.8) {\n"
      "  discard;\n"
      "} else {\n"
      "  float scale = (1.0 - dist);\n"
      "  ambientColor *= scale;\n"
      "  diffuseColor *= scale;\n"
      "}\n";
}

void Visualize::SwitchMapper(int rendertype)
{
  if (rendertype == 0)
  {
    mapper_->SetSplatShaderCode(point_shader_.c_str());
  }
  else if (rendertype == 1)
  {
    mapper_->SetSplatShaderCode(sphere_shader_.c_str());
  }
}

} // namespace vtkexp
