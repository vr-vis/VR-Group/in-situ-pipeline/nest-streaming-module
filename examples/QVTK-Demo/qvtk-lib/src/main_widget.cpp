//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------

#include "qvtk-lib/main_widget.hpp"

#include "vtkGenericOpenGLRenderWindow.h"

#include "QString"
#include "QtWidgets"

namespace vtkexp
{

MainWidget::MainWidget(PointData *points, QVTKOpenGLWidget *parent)
    : QVTKOpenGLWidget(parent)
{
  renderwindow_ = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
  this->SetRenderWindow(renderwindow_.Get());

  style_ = vtkSmartPointer<Interaction>::New();
  style_->SetData(points->GetPolyPoints());
  this->GetInteractor()->SetInteractorStyle(style_);

  vispoints_ = new Visualize(points);
  vispoints_->Initialize(renderwindow_, this->GetInteractor());
}

void MainWidget::mouseReleaseEvent(QMouseEvent *event)
{
  QVTKOpenGLWidget::mouseReleaseEvent(event);
  if (event->button() == Qt::LeftButton && style_->GetNeuronId() != -1)
  {
    QToolTip::showText(this->mapToGlobal(event->pos()),
                       QString::number(style_->GetNeuronValue()));
  }
}

void MainWidget::ChangeRendertype(int rendertype)
{
  vispoints_->SwitchMapper(rendertype);
  style_->SwitchMapper(rendertype);
}

void MainWidget::Rerender() { renderwindow_->Render(); }

} // namespace vtkexp
