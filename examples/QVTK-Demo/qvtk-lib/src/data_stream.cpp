//------------------------------------------------------------------------------
// QVTK-Demo
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include "qvtk-lib/data_stream.hpp"

#include <vector>

namespace vtkexp
{

DataStream::DataStream() { SetUpStream(); }

void DataStream::SetUpStream()
{
  relay_ = new contra::Relay<contra::ZMQTransport>(
      contra::ZMQTransport::Type::CLIENT, "tcp://localhost:5555");

  multimeter_ = new nesci::consumer::NestMultimeter("recordingNode51");
  multimeter_->SetNode(&node_);
}

void DataStream::Update(int time_step)
{
  auto time_step_string = std::to_string(static_cast<double>(time_step));

  const auto received_nodes = relay_->Receive();
  for (auto node : received_nodes)
  {
    node_.update(node);
  }

  if (time_step == -1 && !multimeter_->GetTimesteps().empty())
  {
    time_step_string = multimeter_->GetTimesteps().back();
  }

  mult_values_ = multimeter_->GetTimestepData(time_step_string, "V_m");
}

std::vector<double> DataStream::GetMultValuesAt(int time_step)
{
  Update(time_step);
  return mult_values_;
}

std::vector<double> DataStream::GetTimeSteps()
{
  std::vector<double> ret;
  for (auto value : multimeter_->GetTimesteps())
  {
    ret.push_back(std::stod(value));
  }
  return ret;
}

} // namespace vtkexp
