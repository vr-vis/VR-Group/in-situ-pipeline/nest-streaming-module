get_filename_component(NEST_DIR ${with-nest} DIRECTORY)

execute_process(
  COMMAND ${NEST_CONFIG} --python-executable
  RESULT_VARIABLE RES_VAR
  OUTPUT_VARIABLE PYTHON_EXECUTABLE
  OUTPUT_STRIP_TRAILING_WHITESPACE
)

set(SOURCE_SCRIPT_FILE
  ${CMAKE_CURRENT_LIST_DIR}/run_example.sh.in
)

function(ADD_PYTHON_EXAMPLE)
set(options)
set(oneValueArgs NAME ENTRY_POINT)
set(multiValueArgs PYTHON_FILES)
cmake_parse_arguments(APE "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

set(TARGET_SCRIPT_FILE "${CMAKE_CURRENT_BINARY_DIR}/run_${APE_NAME}_example.sh")
configure_file(${SOURCE_SCRIPT_FILE} ${TARGET_SCRIPT_FILE})

add_custom_target(
  ${APE_NAME}_example
  ALL
  COMMAND chmod "+x" ${TARGET_SCRIPT_FILE}
  COMMAND ${CMAKE_COMMAND} -E copy_if_different ${APE_PYTHON_FILES} ${CMAKE_CURRENT_BINARY_DIR}
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  DEPENDS ${TARGET_SCRIPT_FILE} ${APE_PYTHON_FILES})

  install(
    FILES ${APE_PYTHON_FILES}
    DESTINATION "share/in-situ-pipeline/examples/${APE_NAME}"
  )

  install(
    PROGRAMS ${TARGET_SCRIPT_FILE}
    DESTINATION "share/in-situ-pipeline/examples/${APE_NAME}"
  )

endfunction(ADD_PYTHON_EXAMPLE)